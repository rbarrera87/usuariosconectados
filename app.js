//Require de todos los módulos
var app = require('express')(),
	server = require('http').createServer(app),
	io = require('socket.io').listen(server);

//El server escucha el puerto 8080
server.listen(8088);

//Manejador de routes
//GET /
app.get('/', function (req, res){
	//console.log(res);
	res.sendfile(__dirname + '/index.html');
});

//Manejador de routes
//GET /nuevoUsuario
app.get('/nuevoUsuario', function (req, res){
	//console.log(res);
	res.sendfile(__dirname + '/index.html');
});

//On connection se dispara cada vez que alguien se conecta
io.sockets.on('connection', function (socket){
	socket.on('setNickname', function (data){
		socket.set('nickname', data);
	});
	//Aquí está escuchando el evento agregaUsuario(disparado por el cliente)
	//y hace lo que tiene que hacer, este evento es cachado por el 
	//event loop de node
	socket.on('agregaUsuario', function (data){
		console.log(data);
		socket.get('nickname', function (err, name){
			socket.emit('nuevoUsuario', data + ' - By ' + name);
			socket.broadcast.emit('nuevoUsuario', data + ' - By ' + name);
		});
	});
});
